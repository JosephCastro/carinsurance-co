const CarInsurance = require('../src/carInsurance');
const Product = require('../src/products/product');

const productList = [
  new Product('Normal Coverage', 1, 2),
  new Product('Normal Coverage', 1, 2),
  new Product('Normal Coverage', 1, 2),
];


test('Create a CarInsurance', () => {
  const newCarInsurance = new CarInsurance(productList);
  expect(newCarInsurance.products.length).toBe(3);
});

test('update price for a CarInsurance products', () => {
  const newCarInsurance = new CarInsurance(productList);
  expect(newCarInsurance.updatePrice().length).toBe(3);
  newCarInsurance.products.forEach((x) => {
    expect(x.price).toBe(1);
    expect(x.sellIn).toBe(0);
  });
});
