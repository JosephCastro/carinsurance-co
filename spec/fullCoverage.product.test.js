const FullCoverage = require('../src/products/fullCoverage');

test('Create a product', () => {
  const newProduct = new FullCoverage(10, 9990);
  expect(newProduct.name).toBe('Full Coverage');
  expect(newProduct.sellIn).toBe(10);
  expect(newProduct.price).toBe(50);
});

test('Price is never negative', () => {
  const newProduct = new FullCoverage(10, -9990);
  expect(newProduct.price).toBe(0);
});

test('Price is ok', () => {
  const newProduct = new FullCoverage(10, 30);
  expect(newProduct.price).toBe(30);
});

test('Price is never more than 50', () => {
  const newProduct = new FullCoverage(10, 9990);
  expect(newProduct.price).toBe(50);
});

test('calculate price', () => {
  const newProduct = new FullCoverage(1, 4);
  newProduct.priceCalculation();
  expect(newProduct.price).toBe(5);
});

test('calculate price with losing date', () => {
  const newProduct = new FullCoverage(-1, 4);
  newProduct.priceCalculation();
  expect(newProduct.price).toBe(6);
});

test('updating a price', () => {
  const newProduct = new FullCoverage(5, 4);
  newProduct.updatePrice();
  expect(newProduct.price).toBe(5);
  expect(newProduct.sellIn).toBe(4);
});
