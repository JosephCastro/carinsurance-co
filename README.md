# Car Insurance Products

This code is only a sample of how products could be handled, in relation to the test for software engineer.

The solution simply involves generating a ProductFactory responsible for building all associated products, inheriting from a normal product.

# Adding a new product

If you want to add any product, try in the `generateProduct.js` file to create a product with` ProductFactory.createProduct`, it will contain all the basic rules of a normal product.

If you need to create a product with specific rules, create a class in `src / products` inheriting from Product. So you can add the rules for:

- calculate the price (priceCalculation)
- validate the price (priceValidation)

## How to use

### Generate a 30-days info for a list of products
```
npm run after-30-days
```

### Execute tests with coverage
```
npm test
```

### Execute lint
```
npm run lint
```
