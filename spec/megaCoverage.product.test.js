const MegaCoverage = require('../src/products/megaCoverage');

test('Create a product', () => {
  const newProduct = new MegaCoverage(0);
  expect(newProduct.name).toBe('Mega Coverage');
  expect(newProduct.sellIn).toBe(0);
  expect(newProduct.price).toBe(80);
});

test('Price is always 80', () => {
  const newProduct = new MegaCoverage(-1, 10);
  expect(newProduct.price).toBe(80);
});

test('calculate price', () => {
  const newProduct = new MegaCoverage(0);
  newProduct.priceCalculation();
  expect(newProduct.price).toBe(80);
});

test('calculate price with losing date', () => {
  const newProduct = new MegaCoverage(0);
  newProduct.priceCalculation();
  expect(newProduct.price).toBe(80);
});

test('updating a price', () => {
  const newProduct = new MegaCoverage(0);
  newProduct.updatePrice();
  expect(newProduct.price).toBe(80);
  expect(newProduct.sellIn).toBe(-1);
});
