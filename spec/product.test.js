const Product = require('../src/products/product');

test('Create a product', () => {
  const newProduct = new Product('Test', 10, 9990);
  expect(newProduct.name).toBe('Test');
  expect(newProduct.sellIn).toBe(10);
  expect(newProduct.price).toBe(9990);
});

test('Price is never negative', () => {
  const newProduct = new Product('Test', 10, -9990);
  newProduct.priceValidation();
  expect(newProduct.price).toBe(0);
});

test('Price is ok', () => {
  const newProduct = new Product('Test', 10, 30);
  newProduct.priceValidation();
  expect(newProduct.price).toBe(30);
});

test('Price is never more than 50', () => {
  const newProduct = new Product('Test', 10, 9990);
  newProduct.priceValidation();
  expect(newProduct.price).toBe(50);
});

test('calculate price', () => {
  const newProduct = new Product('Test', 1, 4);
  newProduct.priceCalculation();
  expect(newProduct.price).toBe(3);
});

test('calculate price with losing date', () => {
  const newProduct = new Product('Test', -1, 4);
  newProduct.priceCalculation();
  expect(newProduct.price).toBe(2);
});

test('updating a price', () => {
  const newProduct = new Product('Test', 5, 4);
  newProduct.updatePrice();
  expect(newProduct.price).toBe(3);
  expect(newProduct.sellIn).toBe(4);
});
