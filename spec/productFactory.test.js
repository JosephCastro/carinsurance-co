const ProductFactory = require('../src/productFactory');

test('Create a normal product', () => {
  const product = ProductFactory.createProduct('New Product', 1, 10);
  expect(product.name).toBe('New Product');
});

test('Create a full coverage product', () => {
  const product = ProductFactory.createFullCoverage(1, 10);
  expect(product.name).toBe('Full Coverage');
});

test('Create a mega coverage product', () => {
  const product = ProductFactory.createMegaCoverage(1, 10);
  expect(product.name).toBe('Mega Coverage');
});

test('Create a special full coverage product', () => {
  const product = ProductFactory.createSpecialFullCoverage(1, 10);
  expect(product.name).toBe('Special Full Coverage');
});

test('Create a super sale product', () => {
  const product = ProductFactory.createSuperSale(1, 10);
  expect(product.name).toBe('Super Sale');
});
