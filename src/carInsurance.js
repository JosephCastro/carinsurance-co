class CarInsurance {
  /* istanbul ignore next */
  constructor(products = []) {
    this.products = products;
  }

  updatePrice() {
    return this.products.map((x) => {
      x.updatePrice();
      return x;
    });
  }
}

module.exports = CarInsurance;
