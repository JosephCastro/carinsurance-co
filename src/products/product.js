const FIRST_DECREMENT_PRICE = 1;
const SECOND_DECREMENT_PRICE = 2;
const MINIMUM_PRICE = 0;
const MAXIMUM_PRICE = 50;
const FIRST_SELL_PERIOD = 0;

class Product {
  constructor(name, sellIn, price) {
    this.name = name;
    this.sellIn = sellIn;
    this.price = price;
  }

  priceCalculation() {
    if (this.sellIn > FIRST_SELL_PERIOD) this.price -= FIRST_DECREMENT_PRICE;
    else this.price -= SECOND_DECREMENT_PRICE;
  }

  priceValidation() {
    if (this.price < FIRST_SELL_PERIOD) this.price = MINIMUM_PRICE;
    else if (this.price > MAXIMUM_PRICE) this.price = MAXIMUM_PRICE;
  }

  updatePrice() {
    this.priceCalculation();
    this.priceValidation();
    this.sellIn--;
  }
}

module.exports = Product;
