const Product = require('./product');

const FIRST_SELL_PERIOD = 10;
const SECOND_SELL_PERIOD = 5;
const LAST_SELL_PERIOD = 0;
const BASE_INCREMENT_PRICE = 1;
const FIRST_INCREMENT_PRICE = 2;
const SECOND_INCREMENT_PRICE = 3;
const FINAL_PRICE = 0;

class SpecialFullCoverage extends Product {
  constructor(sellIn, price) {
    super('Special Full Coverage', sellIn, price);
    this.priceValidation();
  }

  isZeroSellPeriod() {
    return this.sellIn > FIRST_SELL_PERIOD;
  }

  isFirstSellPeriod() {
    return this.sellIn > SECOND_SELL_PERIOD && this.sellIn <= FIRST_SELL_PERIOD;
  }

  isSecondSellPeriod() {
    return this.sellIn >= LAST_SELL_PERIOD && this.sellIn <= SECOND_SELL_PERIOD;
  }

  isLastSellPeriod() {
    return this.sellIn < LAST_SELL_PERIOD;
  }

  priceCalculation() {
    if (this.isZeroSellPeriod()) this.price += BASE_INCREMENT_PRICE;
    else if (this.isFirstSellPeriod()) this.price += FIRST_INCREMENT_PRICE;
    else if (this.isSecondSellPeriod()) this.price += SECOND_INCREMENT_PRICE;
    /* istanbul ignore next */
    else if (this.isLastSellPeriod()) this.price = FINAL_PRICE;
  }
}

module.exports = SpecialFullCoverage;
