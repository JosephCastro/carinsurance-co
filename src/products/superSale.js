const Product = require('./product');

const PRICE_DECREMENT = 2;
const LOSE_DATE_DECREMENT_MULTIPLIER = 2;
const FIRST_SELL_PERIOD = 0;

class SuperSale extends Product {
  constructor(sellIn, price) {
    super('Super Sale', sellIn, price);
    this.priceValidation();
  }

  priceCalculation() {
    if (this.sellIn > FIRST_SELL_PERIOD) this.price -= PRICE_DECREMENT;
    else this.price -= (PRICE_DECREMENT * LOSE_DATE_DECREMENT_MULTIPLIER);
  }
}

module.exports = SuperSale;
