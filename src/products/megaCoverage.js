const Product = require('./product');

const BASE_PRICE = 80;

class MegaCoverage extends Product {
  constructor(sellIn) {
    super('Mega Coverage', sellIn, BASE_PRICE);
  }

  priceCalculation() {}

  priceValidation() {
    this.price = BASE_PRICE;
  }
}

module.exports = MegaCoverage;
