const ProductFactory = require('./src/productFactory');
const CarInsurance = require('./src/carInsurance');

const productsAtDayZero = [
  ProductFactory.createProduct('Medium Coverage', 10, 20),
  ProductFactory.createFullCoverage(2, 0),
  ProductFactory.createProduct('Low Coverage', 5, 7),
  ProductFactory.createMegaCoverage(0, 80),
  ProductFactory.createMegaCoverage(-1, 80),
  ProductFactory.createSpecialFullCoverage(15, 20),
  ProductFactory.createSpecialFullCoverage(10, 49),
  ProductFactory.createSpecialFullCoverage(5, 49),
  ProductFactory.createSuperSale(3, 6),
];

const carInsurance = new CarInsurance(productsAtDayZero);
const productPrinter = (product) => {
  console.log(`${product.name}, ${product.sellIn}, ${product.price}`);
};

for (let i = 1; i <= 30; i += 1) {
  console.log(`-------- day ${i} --------`);
  console.log('name, sellIn, price');
  carInsurance.updatePrice().forEach(x => productPrinter(x));
  console.log('');
}
