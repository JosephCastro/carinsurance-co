const SpecialFullCoverage = require('../src/products/specialFullCoverage');

test('Create a product', () => {
  const newProduct = new SpecialFullCoverage(10, 9990);
  expect(newProduct.name).toBe('Special Full Coverage');
  expect(newProduct.sellIn).toBe(10);
  expect(newProduct.price).toBe(50);
});

test('Price is never negative', () => {
  const newProduct = new SpecialFullCoverage(10, -9990);
  expect(newProduct.price).toBe(0);
});

test('Price is ok', () => {
  const newProduct = new SpecialFullCoverage(10, 30);
  expect(newProduct.price).toBe(30);
});

test('Price is never more than 50', () => {
  const newProduct = new SpecialFullCoverage(10, 9990);
  expect(newProduct.price).toBe(50);
});

test('calculate price for zero sell period', () => {
  const newProduct = new SpecialFullCoverage(15, 10);
  newProduct.priceCalculation();
  expect(newProduct.price).toBe(11);
});

test('calculate price for first sell period', () => {
  const newProduct = new SpecialFullCoverage(10, 10);
  newProduct.priceCalculation();
  expect(newProduct.price).toBe(12);
});

test('calculate price for second sell period', () => {
  const newProduct = new SpecialFullCoverage(5, 10);
  newProduct.priceCalculation();
  expect(newProduct.price).toBe(13);
});

test('calculate price with losing date', () => {
  const newProduct = new SpecialFullCoverage(-10, 4);
  newProduct.priceCalculation();
  expect(newProduct.price).toBe(0);
});

test('updating a price', () => {
  const newProduct = new SpecialFullCoverage(15, 4);
  newProduct.updatePrice();
  expect(newProduct.price).toBe(5);
  expect(newProduct.sellIn).toBe(14);
});
