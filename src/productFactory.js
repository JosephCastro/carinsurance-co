const Product = require('./products/product');
const FullCoverage = require('./products/fullCoverage');
const MegaCoverage = require('./products/megaCoverage');
const SpecialFullCoverage = require('./products/specialFullCoverage');
const SuperSale = require('./products/superSale');

class ProductFactory {
  static createProduct(name, sellIn, price) {
    return new Product(name, sellIn, price);
  }

  static createFullCoverage(sellIn, price) {
    return new FullCoverage(sellIn, price);
  }

  static createMegaCoverage(sellIn, price) {
    return new MegaCoverage(sellIn, price);
  }

  static createSpecialFullCoverage(sellIn, price) {
    return new SpecialFullCoverage(sellIn, price);
  }

  static createSuperSale(sellIn, price) {
    return new SuperSale(sellIn, price);
  }
}

module.exports = ProductFactory;
