const Product = require('./product');

const FIRST_PRICE_INCREMENT = 1;
const SECOND_PRICE_INCREMENT = 2;
const FIRST_SELL_PERIOD = 0;

class FullCoverage extends Product {
  constructor(sellIn, price) {
    super('Full Coverage', sellIn, price);
    this.priceValidation();
  }

  priceCalculation() {
    if (this.sellIn > FIRST_SELL_PERIOD) this.price += FIRST_PRICE_INCREMENT;
    else this.price += SECOND_PRICE_INCREMENT;
  }
}

module.exports = FullCoverage;
