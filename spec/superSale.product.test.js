const SuperSale = require('../src/products/superSale');

test('Create a product', () => {
  const newProduct = new SuperSale(10, 9990);
  expect(newProduct.name).toBe('Super Sale');
  expect(newProduct.sellIn).toBe(10);
  expect(newProduct.price).toBe(50);
});

test('Price is never negative', () => {
  const newProduct = new SuperSale(10, -9990);
  expect(newProduct.price).toBe(0);
});

test('Price is ok', () => {
  const newProduct = new SuperSale(10, 30);
  expect(newProduct.price).toBe(30);
});

test('Price is never more than 50', () => {
  const newProduct = new SuperSale(10, 9990);
  expect(newProduct.price).toBe(50);
});

test('calculate price', () => {
  const newProduct = new SuperSale(10, 4);
  newProduct.priceCalculation();
  expect(newProduct.price).toBe(2);
});

test('calculate price with losing date', () => {
  const newProduct = new SuperSale(-10, 10);
  newProduct.priceCalculation();
  expect(newProduct.price).toBe(6);
});

test('updating a price', () => {
  const newProduct = new SuperSale(5, 4);
  newProduct.updatePrice();
  expect(newProduct.price).toBe(2);
  expect(newProduct.sellIn).toBe(4);
});
